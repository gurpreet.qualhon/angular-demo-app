import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public lf: any;
  public hasChanged: boolean = false;
  public loading: boolean = false;
  public credentialsError: string = "";
  defaultUser = {
    username: 'admin@example.com',
    password: 'password'
  }

  loginForm = this.fb.group({
    username: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  });

  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.getFormValues();
    this.onChanges();
  }

  get username(): FormControl {
    return this.loginForm.controls['username'] as FormControl;
  }
  getFormValues() {
    this.lf = this.loginForm.controls;
  }
  onChanges(): void {
    console.log('making changes');

    this.loginForm.valueChanges.subscribe(val => {
      this.hasChanged = true;
      console.log('changes in form');

    });
  }
  onSubmit(): void {
    this.credentialsError = '';
    if (this.loginForm.valid) {
      this.loading = true;
      let form = this.loginForm.value;
      this.loginUser(form);
    }
  }

  private loginUser(form: any): void {
    setTimeout(() => {
      if (form.username == this.defaultUser.username && form.password == this.defaultUser.password) {
        localStorage.setItem('token', 'logged in')
        this.router.navigate(['dashboard']);
      } else {
        this.hasChanged = false;
        this.credentialsError = 'Invalid username or password';
      }
      this.loading = false;
    }, 2000);
  }
}

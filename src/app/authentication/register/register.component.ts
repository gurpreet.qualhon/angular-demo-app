import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  users: any = [];
  duplicateEmail: string = 'somewting went wrong';
  registerForm = this.fb.group({
    first_name: ['Gurpreet', [Validators.required, Validators.minLength(3)]],
    last_name: ['singh'],
    email: ['gurpreet.singh@exampel.com', [Validators.required, Validators.email]],
    password: ['password', [Validators.required, Validators.minLength(6)]]
  });

  constructor(
    private fb: FormBuilder,
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit(): void {
    this.getUsers();
    console.log(this.users);

  }

  onSubmit(): void {
    this.duplicateEmail = '';
    if (this.registerForm.valid) {
      let user = this.registerForm.value;
      if (!this.checkUserExist(user)) {
        console.log(this.registerForm.value);

        this.users.push(this.registerForm.value);
        this.localStorageService.setItem('users', this.users);
      } else {
        this.duplicateEmail = 'This email already exists.'
        this.registerForm.get('email')?.setErrors({ unique: false })
      }

    }
  }

  checkEmailExists(email: string) {

  }
  private checkUserExist(registerUser: any) {
    if (this.users) {
      let userExist = this.users.find((user: any) => user.email === registerUser.email);
      return userExist ? true : false;
    }
    return false;
  }

  private getUsers() {
    this.users = JSON.parse(this.localStorageService.getItem('users')) || [];
  }
}

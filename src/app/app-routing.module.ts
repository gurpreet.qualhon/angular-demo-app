import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './auth/auth.guard';

import { DashboardModule } from './dashboard/dashboard.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TestDASHBAORDComponent } from './test-dashbaord/test-dashbaord.component';

const routes: Routes = [
  { path: '', loadChildren: () => AuthenticationModule },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'test', component: TestDASHBAORDComponent },
  { path: 'dashboard', canActivate: [AuthGuard], loadChildren: () => DashboardModule },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

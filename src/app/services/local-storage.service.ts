import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  setItem(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  getItem(key: any) {
    let users = localStorage.getItem(key);
    return JSON.parse(JSON.stringify(users));
  }
}

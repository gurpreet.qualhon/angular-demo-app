// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBBwbIbhTQ3hGovWhYPOf_WX11R3IwZHw0",
    authDomain: "fir-angular-app-db.firebaseapp.com",
    databaseURL: "https://fir-angular-app-db-default-rtdb.firebaseio.com",
    projectId: "fir-angular-app-db",
    storageBucket: "fir-angular-app-db.appspot.com",
    messagingSenderId: "360788822285",
    appId: "1:360788822285:web:9e27ed76776373d9d54844",
    measurementId: "G-WMDV72VV2Y"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
